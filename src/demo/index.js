import React from 'react'
import TrafficLight from './TrafficLight/TrafficLight'
import TrafficLightHook from './TrafficLight/TrafficLightHook'
import HookDemo from './hook/Test/index'
import Counter from './icestore/index'
export default function App() {
    return (
        <div>
           <Counter/>
        </div>
    )
}
